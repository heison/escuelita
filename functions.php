<?php 
function enqueue_styles_and_scripts(){
	    wp_enqueue_style('style', get_stylesheet_directory_uri().'/style.css');
		// Our own compass stylesheet
		wp_enqueue_style('base-style', get_stylesheet_directory_uri().'/css/base.css');	
		wp_enqueue_style('the-fonts', get_stylesheet_directory_uri().'/css/fonts.css');
		wp_enqueue_style('layout-theme', get_stylesheet_directory_uri().'/css/layout.css');wp_enqueue_script('jquery', get_template_directory_uri().'/js/jquery.min.js', null, null, true );
		wp_enqueue_script('scrollspy', get_template_directory_uri().'/js/scrollspy.js', null, null, true );
		wp_enqueue_script('flexslider', get_template_directory_uri().'/js/jquery.flexslider.js', null, null, true );
		wp_enqueue_script('reveal', get_template_directory_uri().'/js/jquery.reveal.js', null, null, true );
		wp_enqueue_script('gmaps', get_template_directory_uri().'/js/gmaps.js', null, null, true );
		wp_enqueue_script('init', get_template_directory_uri().'/js/init.js', null, null, true );
		wp_enqueue_script('smoothscrolling', get_template_directory_uri().'/js/smoothscrolling.js', null, null, true );		

	}
	add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts');